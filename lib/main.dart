import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokedex/routes/app_page.dart';
import 'package:pokedex/shared/theme/theme.dart';

import 'language/translation_service.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      enableLog: true,
      initialRoute: AppPage.initial,
      getPages: AppPage.routes,
      theme: CustomTheme.customTheme,
      translations: TranslationService(),
      locale: Locale('en', 'US'),
      fallbackLocale: TranslationService.fallbackLocale,
    );
  }
}
