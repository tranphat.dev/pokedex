import 'package:get/get.dart';

class AppProvider extends GetConnect {

  Future<Response> getPokemonArea({required String path}) => get(path);
}
