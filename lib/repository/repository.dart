import 'package:pokedex/model/pokedex_area_model.dart';
import 'package:pokedex/repository/provider.dart';

class PokemonRepository {

  Future<PokemonAreaModel> getPokemonArea({required String areaId}) async {
    return AppProvider().getPokemonArea(path: 'https://pokeapi.co/api/v2/pokedex/$areaId').then((response) {
      return PokemonAreaModel.fromJson(response.body);
    });
  }
}
