import 'package:get/get.dart';
import 'package:pokedex/page/dashboard/controller/dashboard_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    /// Dashboard dependencies
    Get.lazyPut<DashboardController>(() => DashboardController());

  }
}