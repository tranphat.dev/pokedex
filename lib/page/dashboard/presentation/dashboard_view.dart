import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokedex/page/dashboard/controller/dashboard_controller.dart';
import 'package:pokedex/page/home/presentation/home_view.dart';


class DashboardPage extends GetView<DashboardController> {
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        body: IndexedStack(
          index: controller.tabIndex,
          children: [
            HomeView(),
            Container(),
            Container(),
            Container(),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          unselectedItemColor: Colors.black,
          selectedItemColor: Colors.redAccent,
          onTap: controller.changeTabIndex,
          currentIndex: controller.tabIndex,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.white,
          elevation: 0,
          items: [
            _bottomNavigationBarItem(
              icon: Icons.home,
              label: 'Home',
            ),
            _bottomNavigationBarItem(
              icon: Icons.new_releases_sharp,
              label: 'News',
            ),
            _bottomNavigationBarItem(
              icon: Icons.message,
              label: 'Alerts',
            ),
            _bottomNavigationBarItem(
              icon: Icons.person,
              label: 'Account',
            ),
          ],
        ),
      );
    });
  }

  _bottomNavigationBarItem({required IconData icon, required String label}) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      label: label,
    );
  }
}