import 'package:pokedex/model/pokemon_entry_model.dart';

class PokemonAreaModel {
  int id;
  String name;
  List<PokemonEntryModel> pokemonEntries;

  PokemonAreaModel({
    required this.id,
    required this.name,
    required this.pokemonEntries,
  });

  factory PokemonAreaModel.fromJson(Map<String, dynamic> json) {
    return PokemonAreaModel(
      id: json['id'] as int,
      name: json['name'] as String,
      pokemonEntries: (json['pokemon_entries'] as List)
          .map((e) => PokemonEntryModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
  }
}
