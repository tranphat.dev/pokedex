class PokemonEntryModel {
  int entryNumber;
  String namePokemon;
  String urlSpecies;

  PokemonEntryModel({
    required this.entryNumber,
    required this.namePokemon,
    required this.urlSpecies,
  });

  factory PokemonEntryModel.fromJson(Map<String, dynamic> json) {
    return PokemonEntryModel(
      entryNumber: json['entry_number'] as int,
      namePokemon: json['pokemon_species']['name'] as String,
      urlSpecies: json['pokemon_species']['url'] as String,
    );
  }
}