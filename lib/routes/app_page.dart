import 'package:get/get.dart';
import 'package:pokedex/page/dashboard/binding/dashboard_binding.dart';
import 'package:pokedex/page/dashboard/presentation/dashboard_view.dart';

part 'app_route.dart';

class AppPage {
  static const initial = Routes.dashboard;

  static final routes = [
    GetPage(
      name: Routes.dashboard,
      page: () => DashboardPage(),
      binding: DashboardBinding(),
    ),
  ];
}

