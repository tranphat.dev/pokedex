import 'package:flutter/material.dart';

abstract class Styles {
  static const Color greenColor = Color(0xFF4DC2a7);
  static const Color redColor = Color(0xFFFB576A);
  static const Color blueColor = Color(0xFF57ABF2);
  static const Color yellowColor = Color(0xFFFFCE4B);
  static const Color purpleColor = Color(0xFF7C538C);
  static const Color brownColor = Color(0xFFB1736A);
}